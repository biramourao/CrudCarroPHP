<?php

/**
 * @author PC-03
 *
 */
class Carro
{
    private $modelo;
    private $fabricante;
    private $ano;
    private $preco;

    /* function  __construct($modelo, $fabricante, $ano, $preco){
        $this->modelo = $modelo;
        $this->fabricante = $fabricante;
        $this->ano = $ano;
        $this->preco = $preco;
    } */
    
    function __construct(){
        
    }
    
    

    /**
     * @return mixed
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * @return mixed
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $modelo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    /**
     * @param mixed $fabricante
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }
    
    function __toString() {
        return "<br>Fabricante: ".
            $this->getFabricante().
            "<br> Modelo: ".
            $this->getModelo().
            "<br> Ano: ".
            $this->getAno().
            "<br> Preço R$: ".
            $this->getPreco();
    }
}

